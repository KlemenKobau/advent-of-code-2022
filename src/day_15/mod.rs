use std::{fs, ops::Index, collections::HashSet};

pub fn day_15() {

    let contents = fs::read_to_string("src/day_15/input.txt")
        .expect("Should have been able to read the file");

    let sensor_beacon_pairs: Vec<_> = contents.split("\n")
        .map(|x| {
            let parts: Vec<&str> = x.split(" ").collect();

            let sx: i32 = parts[2].index(2..parts[2].len() - 1).parse().unwrap();
            let sy: i32 = parts[3].index(2..parts[3].len() - 1).parse().unwrap();
        
            let bx: i32 = parts[8].index(2..parts[8].len() - 1).parse().unwrap();
            let by: i32 = parts[9].index(2..).parse().unwrap();
        
            ((sx, sy), (bx, by))
        })
        .collect();

    let mut collector = HashSet::new();
    let mut beacons = HashSet::new();
    
    
    for ((sx, sy), (bx, by)) in sensor_beacon_pairs {
        beacons.insert((bx, by));

        let distance = (sx.abs_diff(bx) + sy.abs_diff(by)) as i32;

        for dx in -distance..=distance {
            let point = (sx + dx, 2000000);
            if (sx.abs_diff(point.0) + sy.abs_diff(point.1)) as i32 <= distance {
                collector.insert(point);   
            }
        }
    }

    let matched_places = collector.difference(&beacons)
        .map(|&(_, y)| {y})
        .filter(|&y| y == 2000000)
        .count();
    println!("{}", matched_places);

}

fn check_all_distances(point: (i32, i32), sensors: &Vec<((i32, i32), i32)>) -> bool {

    for &((sx, sy), d) in sensors.iter() {
        let dist = (sx.abs_diff(point.0) + sy.abs_diff(point.1)) as i32;

        if dist <= d {
            return false;
        }
    }

    return true;
}

pub fn day_15_2() {

    let contents = fs::read_to_string("src/day_15/input.txt")
        .expect("Should have been able to read the file");

    let sensors: Vec<_> = contents.split("\n")
        .map(|x| {
            let parts: Vec<&str> = x.split(" ").collect();

            let sx: i32 = parts[2].index(2..parts[2].len() - 1).parse().unwrap();
            let sy: i32 = parts[3].index(2..parts[3].len() - 1).parse().unwrap();
        
            let bx: i32 = parts[8].index(2..parts[8].len() - 1).parse().unwrap();
            let by: i32 = parts[9].index(2..).parse().unwrap();

            let distance = (sx.abs_diff(bx) + sy.abs_diff(by)) as i32;
        
            ((sx, sy), distance)
        })
        .collect();

    let mut col = HashSet::new();

    for i in 0..sensors.len() {
        for j in 0..sensors.len() {
            if i == j {
                continue;
            }

            let ((x1,y1), d1) = sensors[i];
            let ((x2,y2), d2) = sensors[j];
            let d1 = d1 + 1;
            let d2 = d2 + 1;

            for dx in -d1..=d1 {
                let r = d1 - dx.abs();

                for dy in [-r, r] {
                    let (point_x, point_y) = (x1+dx, y1+dy);

                    let distance = point_x.abs_diff(x2) + point_y.abs_diff(y2);

                    let is_in = point_x <= 4000000 && point_x >= 0 && point_y <= 4000000 && point_y >= 0;

                    if d2 == distance as i32 && is_in && check_all_distances((point_x, point_y), &sensors) {
                        col.insert((point_x, point_y));
                    }
                }
            }
        }
    }
    println!("{:?}", col);
}