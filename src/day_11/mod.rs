use std::fs;

#[derive(Debug)]
enum Operation {
    Add(i32),
    Square,
    Multiply(i32)
}

#[derive(Debug)]
struct Monkey {
    pub cache: Vec<i128>,
    pub operation: Operation,

    pub div_num: i32,
    pub t_true: i32,
    pub t_false: i32,
    pub inspections: i32
}

fn process_monkeys(contents: &str) -> Vec<Monkey> {
    let monkeys = contents.split("\n\n");
    let mut monkey_vec: Vec<Monkey> = Vec::new();

    for monkey in monkeys {
        let lines: Vec<&str> = monkey.split("\n")
            .skip(1)
            .map(|x| {x.trim()})
            .collect();

        let items: Vec<_> = lines[0]
            .split_once(":")
            .map(|(_, x)| {x.trim()})
            .unwrap()
            .split(", ")
            .map(|x| {x.parse().unwrap()})
            .collect();

        let operation_line: Vec<&str> = lines[1]
            .split(" ")
            .collect();
        let last_two = operation_line.get(operation_line.len() - 2..operation_line.len()).unwrap();

        let op = last_two[0];
        let y = last_two[1];

        let operation = if y == "old" {
            Operation::Square
        } else {
            let y: i32 = y.parse().unwrap();
            if op == "*" {
                Operation::Multiply(y)
            } else {
                Operation::Add(y)
            }
        };

        let test_num: i32 = lines[2]
            .split(" ")
            .last().unwrap()
            .parse().unwrap();

        let true_throw: i32 = lines[3]
            .split(" ")
            .last().unwrap()
            .parse().unwrap();

        let false_throw: i32 = lines[4]
            .split(" ")
            .last().unwrap()
            .parse().unwrap();

        monkey_vec.push(Monkey {cache: items, operation,
            div_num: test_num, t_true: true_throw, t_false: false_throw, inspections: 0});
    }

    monkey_vec
}

pub fn day_11() {
    let contents = fs::read_to_string("src/day_11/input.txt")
        .expect("Should have been able to read the file");

    let mut monkeys = process_monkeys(&contents);

    let modulo: i32 = monkeys.iter()
        .map(|x| {x.div_num})
        .reduce(|a, b| {a * b})
        .unwrap();

    let mut i = 0;

    while i < 10000 * monkeys.len() {
        let monkeys_len = monkeys.len();

        let mut operations_to_perform: Vec<(usize, i128)> = Vec::new();

        let monkey = &mut monkeys[i % monkeys_len];
        for &item in monkey.cache.iter() {
            let new_item = match monkey.operation {
                Operation::Add(t) => item + t as i128,
                Operation::Square => item * item,
                Operation::Multiply(t) => item * t as i128
            };

            let after_bored = new_item % modulo as i128;
            let is_divisible = after_bored % monkey.div_num as i128 == 0;
            let target_monkey_index: usize = if is_divisible {
                monkey.t_true as usize
            } else {
                monkey.t_false as usize
            };
            monkey.inspections += 1;

            operations_to_perform.push((target_monkey_index, after_bored));
        }
        monkey.cache.clear();

        for &(target, item) in operations_to_perform.iter() {
            let mut target_monkey = &mut monkeys[target];
            target_monkey.cache.push(item);
        }
        operations_to_perform.clear();

        i = i + 1;
    }

    let mut inspections: Vec<_> = monkeys.iter()
        .map(|x| { x.inspections })
        .collect();
    inspections.sort_unstable_by(|a, b| {b.cmp(a)});

    println!("{}", inspections[0] as i128 * inspections[1] as i128);
}