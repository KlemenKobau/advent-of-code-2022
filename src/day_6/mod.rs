use std::collections::HashSet;
use std::fs;

pub fn day_6() {
    let contents = fs::read_to_string("src/day_6/input.txt")
        .expect("Should have been able to read the file");

    let search_len = 14;

    for i in search_len - 1..contents.len() {
        let substring = &contents[i + 1 - search_len..i + 1];
        let set: HashSet<char> = HashSet::from_iter(substring.chars());

        if set.len() == search_len {
            println!("{}", i + 1);
            break;
        }
    }
}