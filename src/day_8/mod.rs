use std::fs;

pub fn day_8() {
    let contents = fs::read_to_string("src/day_8/input.txt")
        .expect("Should have been able to read the file");

    let numbers = contents
        .split("\n")
        .map(|x| {
            x.chars()
                .map(|x1| {x1.to_digit(10).unwrap()})
                .collect::<Vec<_>>()
        }).collect::<Vec<Vec<_>>>();

    let mut visible: Vec<Vec<bool>> = Vec::new();

    for line in numbers.iter() {
        let mut b_line_vec = Vec::new();

        for _ in 0..line.len() {
            b_line_vec.push(false);
        }

        visible.push(b_line_vec);
    }

    // up down
    for j in 0..numbers[0].len() {
        let mut tallest: i32 = -1;
        for i in 0..numbers.len() {
            let element = numbers[i][j] as i32;
            if element > tallest {
                tallest = element;
                visible[i][j] = true;
            }
        }
    }

    // left right
    for i in 0..numbers.len() {
        let mut tallest: i32 = -1;
        for j in 0..numbers[0].len() {
            let element = numbers[i][j] as i32;
            if element > tallest {
                tallest = element;
                visible[i][j] = true;
            }
        }
    }

    // down up
    for j in 0..numbers[0].len() {
        let mut tallest: i32 = -1;
        for i in (0..numbers.len()).rev() {
            let element = numbers[i][j] as i32;
            if element > tallest {
                tallest = element;
                visible[i][j] = true;
            }
        }
    }

    // right left
    for i in 0..numbers.len() {
        let mut tallest: i32 = -1;
        for j in (0..numbers[0].len()).rev() {
            let element = numbers[i][j] as i32;
            if element > tallest {
                tallest = element;
                visible[i][j] = true;
            }
        }
    }

    let visible_count: i32 = visible.iter()
        .map(|x| {x.iter().map(|&x2| {if x2 { 1 } else { 0 }}).sum::<i32>()})
        .sum::<i32>();
    println!("{visible_count}");
}

pub fn day_8_2() {
    let contents = fs::read_to_string("src/day_8/input.txt")
        .expect("Should have been able to read the file");

    let numbers = contents
        .split("\n")
        .map(|x| {
            x.chars()
                .map(|x1| { x1.to_digit(10).unwrap() })
                .collect::<Vec<_>>()
        }).collect::<Vec<Vec<_>>>();

    let mut visible: Vec<Vec<i32>> = Vec::new();

    for _ in 0..numbers.len() {
        let mut b_line_vec = Vec::new();

        for _ in 0..numbers[0].len() {
            b_line_vec.push(0);
        }

        visible.push(b_line_vec);
    }

    for i in 0..numbers.len() {
        for j in 0..numbers[0].len() {
            // main brute force approach
            // rust banzai

            let current_size = numbers[i][j];

            let mut smaller_left = 0;
            for l in (0..j).rev() {
                if numbers[i][l] < current_size {
                    smaller_left += 1;
                } else {
                    smaller_left += 1;
                    break;
                }
            }

            let mut smaller_right = 0;
            for r in j+1..numbers[0].len() {
                if numbers[i][r] < current_size {
                    smaller_right += 1;
                } else {
                    smaller_right += 1;
                    break;
                }
            }

            let mut smaller_up = 0;
            for u in (0..i).rev() {
                if numbers[u][j] < current_size {
                    smaller_up += 1;
                } else {
                    smaller_up += 1;
                    break;
                }
            }

            let mut smaller_down = 0;
            for d in i+1..numbers.len() {
                if numbers[d][j] < current_size {
                    smaller_down += 1;
                } else {
                    smaller_down += 1;
                    break;
                }
            }

            visible[i][j] = smaller_up * smaller_down * smaller_left * smaller_right;
        }
    }

    let max = visible.iter()
        .map(|x| {x.iter().max().unwrap()})
        .max().unwrap();

    println!("{max}");
}