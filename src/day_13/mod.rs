use std::{fs, ops::Index, cmp::Ordering};

#[derive(Debug, Clone)]
enum Signal {
    Number(i32),
    Array(Vec<Signal>),
    Empty 
}

enum Decision {
    Undecided,
    Left,
    Right
}

fn parse(code: &str) -> Signal {
    if code.starts_with("[") {
        let inner_nums = code.index(1..code.len());
        let mut signal_arr = Vec::new();

        let mut amount_in = 0;
        let mut current_start = 0;

        for (i, c) in inner_nums.chars().enumerate() {
            if c == '[' {
                amount_in += 1;
            } else if c == ']' && amount_in == 0 {
                let sub = inner_nums.index(current_start..i);
                signal_arr.push(parse(sub));
            } else if c == ']' {
                amount_in -= 1;
            } else if c == ',' && amount_in == 0 {
                let sub = inner_nums.index(current_start..i);
                signal_arr.push(parse(sub));
                current_start = i + 1;
            }
        }

        Signal::Array(signal_arr)
    } else if code.len() == 0 {
        Signal::Empty
    } else {
        let num = code.parse().unwrap();
        Signal::Number(num)
    }
}

fn compare(left: &Signal, right: &Signal) -> Decision {
    match left {
        Signal::Number(nl) => {
            match right {
                Signal::Number(nr) => {
                    if nl < nr {
                        Decision::Right
                    } else if nl == nr {
                        Decision::Undecided
                    } else {
                        Decision::Left
                    }
                },
                Signal::Array(ar) => {
                    let mut vec = Vec::new();
                    vec.push(Signal::Number(*nl));

                    compare(&Signal::Array(vec), &Signal::Array(ar.to_vec()))
                },
                Signal::Empty => {
                    Decision::Left
                }
            }
        },
        Signal::Array(al) => {
            match right {
                Signal::Number(nr) => {
                    let mut vec = Vec::new();
                    vec.push(Signal::Number(*nr));

                    compare(&Signal::Array(al.to_vec()), &Signal::Array(vec))
                },
                Signal::Array(ar) => {
                    for (i, l) in al.iter().enumerate() {
                        let ro = ar.get(i);
                        if ro.is_none() {
                            return Decision::Left;
                        }

                        let r = ro.unwrap();
                        match compare(l, r) {
                            Decision::Undecided => {},
                            Decision::Left => return Decision::Left,
                            Decision::Right => return Decision::Right
                        }
                    }
                    if al.len() < ar.len() {
                        return Decision::Right;
                    }
                    return Decision::Undecided;
                },
                Signal::Empty => Decision::Left
            }
        },
        Signal::Empty => {
            match right {
                Signal::Number(_) => Decision::Right,
                Signal::Array(_) => Decision::Right,
                Signal::Empty => Decision::Undecided
            }
        },
    }
}

fn create_2_mark() -> Signal {
    let mut inner_vec = Vec::new();
    inner_vec.push(Signal::Number(2));

    let mut outer_vec = Vec::new();
    outer_vec.push(Signal::Array(inner_vec));

    Signal::Array(outer_vec)
}

fn create_6_mark() -> Signal {
    let mut inner_vec = Vec::new();
    inner_vec.push(Signal::Number(6));

    let mut outer_vec = Vec::new();
    outer_vec.push(Signal::Array(inner_vec));

    Signal::Array(outer_vec)
}

pub fn day_13() {
    
    let contents = fs::read_to_string("src/day_13/input.txt")
        .expect("Should have been able to read the file");

    let pairs = contents.split("\n\n");

    let mut sum = 0;
    let mut all_signals = Vec::new();
    all_signals.push(create_2_mark());
    all_signals.push(create_6_mark());
 
    for (i, pair) in pairs.enumerate() {
        let (left, right) = pair.split_once("\n").unwrap();

        let left_signal = parse(left);
        let right_signal = parse(right);
    
        match compare(&left_signal, &right_signal) {
            Decision::Right => sum += i + 1,
            Decision::Left => {},
            Decision::Undecided => panic!()
        }

        all_signals.push(left_signal);
        all_signals.push(right_signal);
    }

    all_signals.sort_unstable_by(|a, b| {
        match compare(a, b) {
            Decision::Undecided => panic!(),
            Decision::Left => Ordering::Greater,
            Decision::Right =>  Ordering::Less,
        }
    });

    println!("{sum}");

    let mut decoder_key = 1;
    for (i, signal) in all_signals.iter().enumerate() {
        match signal {
            Signal::Array(a) => {
                if a.len() != 1 {
                    continue;
                }
                let first_el = &a[0];
                match first_el {
                    Signal::Array(a) => {
                        if a.len() != 1 {
                            continue;
                        }
                        let first_el = &a[0];
                        match first_el {
                            Signal::Number(n) => {
                                if *n == 2 || *n == 6 {
                                    decoder_key *= i + 1;
                                }
                            },
                            _ => continue
                        }
                    },
                    _ => continue,
                }
            },
            _ => continue
        }
    }

    println!("{decoder_key}");

}