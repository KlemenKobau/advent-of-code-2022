use std::{
    collections::{HashMap, VecDeque},
    fs,
};

const TIME_BEFORE_ERUPTION: i32 = 30; // minutes
const START_VALVE: &str = "AA";

struct ValveNetwork {
    valves: HashMap<String, Valve>,
}

impl ValveNetwork {
    fn add_valve(&mut self, valve_desc: &str) {
        let valve = ValveNetwork::create_valve(valve_desc);
        let name: String = (&valve.valve_name).into();
        self.valves.insert(name, valve);
    }

    fn create_valve(valve_desc: &str) -> Valve {
        let (valve, tunnels) = valve_desc.split_once(";").unwrap();
        let mut split = valve.split(" ").enumerate();
        let valve_name: String = split.find(|&(i, _)| i == 1).unwrap().1.into();
        let rate: i32 = split
            .find(|&(i, _)| i == 4)
            .and_then(|x| x.1.split_once("="))
            .map(|x| x.1)
            .and_then(|x| x.parse().ok())
            .unwrap();

        let nei: Option<Vec<String>> = tunnels
            .split_once("valve ")
            .map(|x| x.1.into())
            .map(|x| vec![x]);

        let nei_mul: Option<Vec<String>> = tunnels
            .split_once("valves ")
            .map(|x| x.1)
            .map(|x| x.split(", ").map(|x| x.into()).collect());

        let neighbours = nei.or(nei_mul).unwrap();

        Valve {
            valve_name,
            flow_rate: rate,
            connected_valves: neighbours,
        }
    }

    fn new() -> Self {
        Self {
            valves: HashMap::new(),
        }
    }

    fn calculate_flow(&self, open_valves: &[String]) -> i32 {
        open_valves
            .iter()
            .map(|x| self.valves.get(x).unwrap().flow_rate)
            .sum()
    }

    fn solve(&self) -> i32 {
        let mut bfs_queue = VecDeque::new();
        let mut memory = Memory::new();

        let initial_solution = NetworkSolution {
            current_valve: self.valves.get(START_VALVE).unwrap(),
            open_valves: Vec::new(),
            current_time: 0,
            current_flow: 0,
        };
        bfs_queue.push_back(initial_solution);

        while let Some(next_item) = bfs_queue.pop_front() {
            println!(
                "{}:{}:{}",
                next_item.current_time,
                bfs_queue.len(),
                memory.snapshots.len()
            );

            if matches!(memory.save_memory(&next_item), MemoryFeedback::Exists) {
                continue;
            }

            if next_item.current_time > TIME_BEFORE_ERUPTION {
                continue;
            }

            for nei in &next_item.current_valve.connected_valves {
                let nei = self.valves.get(nei).unwrap();

                let mut next_position = next_item.clone();
                next_position.current_valve = nei;
                next_position.current_time += 1;
                next_position.current_flow += self.calculate_flow(&next_item.open_valves);
                bfs_queue.push_back(next_position);
            }

            if !next_item
                .open_valves
                .contains(&next_item.current_valve.valve_name)
            {
                let mut opened_valve = next_item.clone();
                opened_valve.current_flow += self.calculate_flow(&next_item.open_valves);
                opened_valve.current_time += 1;

                opened_valve
                    .open_valves
                    .push(next_item.current_valve.valve_name.clone());
                bfs_queue.push_back(opened_valve);
            }
        }

        todo!();
    }
}

#[derive(Debug)]
struct Valve {
    valve_name: String,
    flow_rate: i32,
    connected_valves: Vec<String>,
}

#[derive(Debug, Clone)]
struct NetworkSolution<'a> {
    current_valve: &'a Valve,
    open_valves: Vec<String>,
    current_time: i32,
    current_flow: i32,
}

struct Memory {
    snapshots: HashMap<String, i32>,
}

impl Memory {
    fn new() -> Self {
        Memory {
            snapshots: HashMap::new(),
        }
    }

    fn save_memory(&mut self, solution: &NetworkSolution) -> MemoryFeedback {
        let hashed_name: String = format!(
            "{}:{}",
            solution.open_valves.join(","),
            solution.current_valve.valve_name
        );

        if !self.snapshots.contains_key(&hashed_name)
            || self
                .snapshots
                .get(&hashed_name)
                .map(|x| x < &solution.current_flow)
                .unwrap()
        {
            self.snapshots.insert(hashed_name, solution.current_flow);

            MemoryFeedback::Saved
        } else {
            MemoryFeedback::Exists
        }
    }
}

enum MemoryFeedback {
    Saved,
    Exists,
}

pub fn day_16() {
    let contents =
        fs::read_to_string("src/day_16/input.txt").expect("Should have been able to read the file");

    let network = parse_valves(&contents);
    let total_flow = network.solve();

    println!("{total_flow}");
}

fn parse_valves(data: &str) -> ValveNetwork {
    let mut network = ValveNetwork::new();

    for data_row in data.split("\n") {
        network.add_valve(data_row);
    }

    network
}
