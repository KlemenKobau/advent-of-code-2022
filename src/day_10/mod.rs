use std::fs;

pub fn day_10() {
    let contents = fs::read_to_string("src/day_10/input.txt")
        .expect("Should have been able to read the file");

    let lines = contents.split("\n").collect::<Vec<_>>();

    let mut register: i32 = 1;
    let mut current_cycle: i32   = 1;
    let mut line_num = 0;

    let mut is_executing = false;
    let mut amount_to_add: i32 = 0;


    while current_cycle <= 240 {
        let current_instruction = lines[line_num % lines.len()];

        if (register - (current_cycle - 1) % 40).abs() <= 1 {
            print!("#");
        } else {
            print!(".");
        }

        if current_cycle % 40 == 0  {
            println!();
        }

        if is_executing {
            register += amount_to_add;
            is_executing = false;
        } else if current_instruction == "noop"{
            line_num += 1;
        } else {
            amount_to_add = current_instruction.split_once(" ")
                .map(|(_, a)| {a.parse::<i32>().unwrap()})
                .unwrap();
            is_executing = true;
            line_num += 1;
        }

        current_cycle += 1;
    }

}