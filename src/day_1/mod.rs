use std::fs;

pub fn day_1() {
    let contents = fs::read_to_string("src/day_1/input.txt")
        .expect("Should have been able to read the file");

    let split: Vec<&str> = contents.split("\n\n").collect();
    let sums: Vec<i32> = split.into_iter()
        .map(|x| {x.split("\n")
            .map(|num| {num.parse::<i32>().unwrap()})
            .sum::<i32>()})
        .collect();
    let max = sums.iter().max().unwrap();
    println!("{max:?}");
}

pub fn day_1_2() {
    let contents = fs::read_to_string("src/day_1/input.txt")
        .expect("Should have been able to read the file");

    let split: Vec<&str> = contents.split("\n\n").collect();
    let mut sums: Vec<i32> = split.into_iter()
        .map(|x| {x.split("\n")
            .map(|num| {num.parse::<i32>().unwrap()})
            .sum::<i32>()})
        .collect();
    sums.sort_unstable_by(|a, b| {b.cmp(a)});

    let top_three: i32 = sums[..3].iter().sum();
    println!("{top_three:?}")
}