use std::fs;

fn prepare_matrix(contents: &str) -> [[bool; 900]; 900] {
    let mut lowest = 0;

    // 900 hardcoded (oh well)
    let mut matrix = [[false; 900]; 900];

    let lines = contents.split("\n");
    for line in lines {
        let mut coordinates = line.split(" -> ")
            .map(|x| x.split_once(",").unwrap())
            .map(|(x, y)| (x.parse::<usize>().unwrap(), y.parse::<usize>().unwrap()));

        let (mut old_x, mut old_y) = coordinates.next().unwrap();
        
        for (x, y) in coordinates {
            if x == old_x {
                let sy = y.min(old_y);
                let by = y.max(old_y);

                for i in sy..=by {
                    matrix[i][x] = true;
                }

            } else {
                let sx = x.min(old_x);
                let bx = x.max(old_x);

                for i in sx..=bx {
                    matrix[y][i] = true;
                }
            }

            old_x = x;
            old_y = y;

            lowest = lowest.max(y);
        }
    }

    for i in 0..matrix.len() {
        matrix[lowest + 2][i] = true;
    }

    matrix
}

pub fn day_14() {

    let contents = fs::read_to_string("src/day_14/input.txt")
        .expect("Should have been able to read the file");

    let mut matrix = prepare_matrix(&contents);
    let sand_source = (0, 500);
    
    let mut sand_amount = 0;
    let mut sand_position = sand_source;
    
    loop {
        if !matrix[sand_position.0 + 1][sand_position.1] {
            // directly under
            sand_position = (sand_position.0 + 1, sand_position.1);
        
        } else if !matrix[sand_position.0 + 1][sand_position.1 - 1] {
            // under left
            sand_position = (sand_position.0 + 1, sand_position.1 - 1);

        } else if !matrix[sand_position.0 + 1][sand_position.1 + 1] {
            // under right
            sand_position = (sand_position.0 + 1, sand_position.1 + 1);
        
        } else {
            // should settle
            matrix[sand_position.0][sand_position.1] = true;
            sand_amount += 1;

            if sand_position == sand_source {
                break;
            }

            sand_position = sand_source;
        }
    }

    println!("{sand_amount}");
}