use std::fs;

enum Outcome {
    Loss,
    Win,
    Draw
}

impl Outcome {
    fn from_string(s: &str) -> Outcome {
        if s == "Y" {
            Outcome::Draw
        } else if s == "X" {
            Outcome::Loss
        } else if s == "Z" {
            Outcome::Win
        } else {
            panic!();
        }
    }
}

enum Opponent {
    Rock,
    Paper,
    Scissors
}

impl Opponent {
    fn from_string(s: &str) -> Opponent {
        if s == "A" {
            Opponent::Rock
        } else if s == "B" {
            Opponent::Paper
        } else if s == "C" {
            Opponent::Scissors
        } else {
            panic!();
        }
    }

    fn player_choice(&self, outcome: &Outcome) -> Player {
        match self {
            Opponent::Rock => {
                match outcome {
                    Outcome::Loss => Player::Scissors,
                    Outcome::Win => Player::Paper,
                    Outcome::Draw => Player::Rock
                }
            }
            Opponent::Paper => {
                match outcome {
                    Outcome::Loss => Player::Rock,
                    Outcome::Win => Player::Scissors,
                    Outcome::Draw => Player::Paper
                }
            }
            Opponent::Scissors => {
                match outcome {
                    Outcome::Loss => Player::Paper,
                    Outcome::Win => Player::Rock,
                    Outcome::Draw => Player::Scissors
                }
            }
        }
    }
}

enum Player {
    Rock,
    Paper,
    Scissors
}

impl Player {
    fn from_string(s: &str) -> Player {
        if s == "Y" {
            Player::Paper
        } else if s == "X" {
            Player::Rock
        } else if s == "Z" {
            Player::Scissors
        } else {
            panic!();
        }
    }

    fn get_value(&self) -> i32 {
        match self {
            Player::Rock => 1,
            Player::Paper => 2,
            Player::Scissors => 3
        }
    }
}

fn fight(opponent: &Opponent, player: &Player) -> i32 {
    match opponent {
        Opponent::Rock => {
            match player {
                Player::Rock => 3,
                Player::Paper => 6,
                Player::Scissors => 0
            }
        }
        Opponent::Paper => {
            match player {
                Player::Rock => 0,
                Player::Paper => 3,
                Player::Scissors => 6
            }
        }
        Opponent::Scissors => {
            match player {
                Player::Rock => 6,
                Player::Paper => 0,
                Player::Scissors => 3
            }
        }
    }
}

pub fn day_two() {
    let contents = fs::read_to_string("src/day_2/input.txt")
        .expect("Should have been able to read the file");

    let lines = contents.split("\n");

    let mut score = 0;

    for line in lines {
        let split_line: Vec<&str> = line.split(" ").collect();
        let opponent = Opponent::from_string(split_line.get(0).unwrap());
        let player = Player::from_string(split_line.get(1).unwrap());

        let outcome = fight(&opponent, &player);
        let play_score = player.get_value();

        score = score + outcome + play_score;
    }

    println!("{score}");
}

pub fn day_two_2() {
    let contents = fs::read_to_string("src/day_2/input.txt")
        .expect("Should have been able to read the file");

    let lines = contents.split("\n");

    let mut score = 0;

    for line in lines {
        let split_line: Vec<&str> = line.split(" ").collect();
        let opponent = Opponent::from_string(split_line.get(0).unwrap());
        let outcome = Outcome::from_string(split_line.get(1).unwrap());
        let player_choice = opponent.player_choice(&outcome);

        let outcome_score = fight(&opponent, &player_choice);
        let play_score = player_choice.get_value();

        score = score + outcome_score + play_score;
    }

    println!("{score}");
}