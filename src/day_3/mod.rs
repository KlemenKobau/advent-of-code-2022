use std::borrow::Borrow;
use std::collections::HashSet;
use std::fs;

const PRIORITY_ARRAY: &str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

fn get_priority(letter: &char) -> i32 {
    i32::try_from(PRIORITY_ARRAY.chars()
        .position(|x| { x == *letter }).unwrap() + 1).unwrap()
}

pub fn day_3() {
    let contents = fs::read_to_string("src/day_3/input.txt")
        .expect("Should have been able to read the file");
    let lines = contents.split("\n");

    let mut sum = 0;

    for line in lines {
        let line_length = line.len();
        let (first, second) = line.split_at(line_length / 2);

        let first_set: HashSet<char> = HashSet::from_iter(first.chars());
        let second_set: HashSet<char> = HashSet::from_iter(second.chars());

        let mut common = first_set.intersection(&second_set);
        let x = common.next().unwrap();

        sum = sum + get_priority(x);
    }

    println!("{sum}");
}

pub fn day_3_2() {
    let contents = fs::read_to_string("src/day_3/input.txt")
        .expect("Should have been able to read the file");
    let lines = contents.split("\n");

    let mut sum = 0;

    let mut first: HashSet<char> = HashSet::new();
    let mut second: HashSet<char> = HashSet::new();
    let mut third: HashSet<char> = HashSet::new();

    for (i, line) in lines.enumerate() {
        if i % 3 == 0 {
            first.clear();
            first.extend(line.chars());
        } else if i % 3 == 1 {
            second.clear();
            second.extend(line.chars());
        } else {
            third.clear();
            third.extend(line.chars());

            let common: HashSet<char> = HashSet::from_iter(first.intersection(&second)
                .map(|x1| {x1.to_owned()}));
            let mut common_all = common.intersection(&third);
            let x = common_all.next().unwrap();

            sum = sum + get_priority(x);
        }
    }

    println!("{sum}");
}