use std::collections::VecDeque;
use std::fs;

const LETTERS: &str = "abcdefghijklmnopqrstuvwxyz";

fn check_heights(start: (usize, usize), end: (usize, usize), heights: &Vec<Vec<i32>>) -> bool {
    heights[end.0][end.1] - heights[start.0][start.1] < 2
}

pub fn day_12() {
    let contents = fs::read_to_string("src/day_12/input.txt")
        .expect("Should have been able to read the file");

    let letters: Vec<Vec<char>> = contents.split("\n")
        .map(|x| {x.chars().collect()})
        .collect();

    let mut heights: Vec<Vec<i32>> = Vec::new();
    let mut min_paths: Vec<Vec<i32>> = Vec::new();

    let mut start = (0, 0);
    let mut end = (0, 0);

    for (i, line) in letters.iter().enumerate() {
        let mut row = Vec::new();
        let mut paths_row = Vec::new();

        for (j, &letter) in line.iter().enumerate() {
            if letter == 'S' {
                start = (i, j);
                row.push(0);
            } else if letter == 'E' {
                end = (i, j);
                row.push(25);
            } else {
                row.push(LETTERS.find(letter).unwrap() as i32);
            }
            paths_row.push(-1);
        }
        heights.push(row);
        min_paths.push(paths_row);
    }

    let mut candidates: VecDeque<(usize, usize, usize)> = VecDeque::new();
    candidates.push_back((0, start.0, start.1));

    let mut i = 0;
    while i < 100000 && !candidates.is_empty() {
        i += 1;

        candidates.make_contiguous().sort_unstable_by_key(|x| {x.0});

        let (prior, x, y) = candidates.pop_front().unwrap();
        if end == (x, y) {
            println!("{}", prior);
            break;
        }

        if min_paths[x][y] != -1 {
            continue;
        }

        min_paths[x][y] = prior as i32;
        let new_prior = prior + 1;

        if y > 0 && check_heights((x, y), (x, y - 1), &heights) {
            candidates.push_back((new_prior, x, y - 1));
        }
        if y < min_paths[0].len() - 1 && check_heights((x, y), (x, y + 1), &heights) {
            candidates.push_back((new_prior, x, y + 1));
        }
        if x > 0 && check_heights((x, y), (x - 1, y), &heights) {
            candidates.push_back((new_prior, x - 1, y));
        }
        if x < min_paths.len() - 1 && check_heights((x, y), (x + 1, y), &heights) {
            candidates.push_back((new_prior, x + 1, y));
        }
    }
}

pub fn day_12_2() {
    let contents = fs::read_to_string("src/day_12/input.txt")
        .expect("Should have been able to read the file");

    let letters: Vec<Vec<char>> = contents.split("\n")
        .map(|x| {x.chars().collect()})
        .collect();

    let mut heights: Vec<Vec<i32>> = Vec::new();
    let mut min_paths: Vec<Vec<i32>> = Vec::new();

    let mut starts: Vec<_> = Vec::new();
    let mut end = (0, 0);

    for (i, line) in letters.iter().enumerate() {
        let mut row = Vec::new();
        let mut paths_row = Vec::new();

        for (j, &letter) in line.iter().enumerate() {
            if letter == 'S' || letter == 'a' {
                row.push(0);
                starts.push((i, j));
            } else if letter == 'E' {
                end = (i, j);
                row.push(25);
            } else {
                row.push(LETTERS.find(letter).unwrap() as i32);
            }
            paths_row.push(-1);
        }
        heights.push(row);
        min_paths.push(paths_row);
    }

    let mut res = Vec::new();

    for &(sx, sy) in starts.iter() {
        let mut candidates: VecDeque<(usize, usize, usize)> = VecDeque::new();
        candidates.push_back((0, sx, sy));
        let mut paths = min_paths.to_vec();

        let mut i = 0;
        while i < 100000 && !candidates.is_empty() {
            i += 1;

            candidates.make_contiguous().sort_unstable_by_key(|x| {x.0});

            let (prior, x, y) = candidates.pop_front().unwrap();
            if end == (x, y) {
                res.push(prior);
                break;
            }

            if paths[x][y] != -1 {
                continue;
            }

            paths[x][y] = prior as i32;
            let new_prior = prior + 1;

            if y > 0 && check_heights((x, y), (x, y - 1), &heights) {
                candidates.push_back((new_prior, x, y - 1));
            }
            if y < min_paths[0].len() - 1 && check_heights((x, y), (x, y + 1), &heights) {
                candidates.push_back((new_prior, x, y + 1));
            }
            if x > 0 && check_heights((x, y), (x - 1, y), &heights) {
                candidates.push_back((new_prior, x - 1, y));
            }
            if x < min_paths.len() - 1 && check_heights((x, y), (x + 1, y), &heights) {
                candidates.push_back((new_prior, x + 1, y));
            }
        }
    }

    println!("{:?}", res.iter().min())
}