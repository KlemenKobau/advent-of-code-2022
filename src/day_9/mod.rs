use std::collections::HashSet;
use std::fs;

pub fn day_9() {
    let contents = fs::read_to_string("src/day_9/input.txt")
        .expect("Should have been able to read the file");

    let lines = contents.split("\n");

    let mut v: Vec<(i32, i32)> = Vec::new();
    for _ in 0..10 {
        v.push((0,0));
    }


    let mut visited = HashSet::new();
    visited.insert((0,0));

    for line in lines {
        let (command, amount) = line.split_once(" ")
            .map(|(c, a)| {(c, a.parse::<i32>().unwrap())})
            .unwrap();

        for _ in 0..amount {

            match command {
                "D" => {
                    v[0].0 += 1;
                },
                "U" => {
                    v[0].0 -= 1;
                }
                "L" => {
                    v[0].1 -= 1;
                }
                "R" => {
                    v[0].1 += 1;
                }
                _ => panic!()
            }

            for tail in 1..10 {
                let y_distance: i32 = v[tail - 1].0 - v[tail].0;
                let x_distance: i32 = v[tail - 1].1 - v[tail].1;

                if y_distance.abs() > 1 || x_distance.abs() > 1{
                    if y_distance > 0 {
                        v[tail].0 += 1;
                    } else if y_distance < 0 {
                        v[tail].0 -= 1;
                    }

                    if x_distance > 0 {
                        v[tail].1 += 1;
                    } else if x_distance < 0 {
                        v[tail].1 -= 1;
                    }

                    if tail == 9 {
                        visited.insert(v[9]);
                    }
                }
            }
        }

    }

    println!("{}", visited.len());
}