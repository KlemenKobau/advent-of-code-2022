use std::fs;

fn get_columns(crates: &str) -> Vec<Vec<char>> {
    let crate_rows = crates.split("\n")
        .map(|x1| {x1.to_owned()})
        .collect::<Vec<String>>();

    let number_of_columns: i32 = crate_rows.last().unwrap()
        .trim()
        .split("   ")
        .last().unwrap()
        .parse().unwrap();

    let mut columns: Vec<Vec<char>> = Vec::new();

    for _ in 0..number_of_columns {
        columns.push(Vec::new());
    }

    for line in crate_rows.get(..crate_rows.len() - 1).unwrap() {
        let boxes = line.trim_end()
            .chars()
            .skip(1)
            .step_by(4)
            .collect::<Vec<char>>();

        for (i, bo) in boxes.iter().enumerate() {
            if *bo != ' ' {
                columns[i].insert(0, *bo);
            }
        }
    }

    columns
}

fn parse_instructions(instructions: &str) -> Vec<[usize; 3]> {
    let mut inst_vec = Vec::new();

    for line in instructions.split("\n") {
        let split: Vec<&str> = line.split(" ").collect();

        let moved_amount = split[1].parse().unwrap();
        let from: usize = split[3].parse().unwrap();
        let to: usize = split[5].parse().unwrap();

        inst_vec.push([moved_amount, from - 1, to - 1]);
    }

    inst_vec
}

pub fn day_5() {
    let contents = fs::read_to_string("src/day_5/input.txt")
        .expect("Should have been able to read the file");
    let (crates, instructions) = contents.split_once("\n\n").unwrap();

    let mut columns = get_columns(crates);
    let instructions = parse_instructions(instructions);

    for [amount, from, to] in instructions {
        for _ in 0..amount {
            let x = &columns[from].pop().unwrap();
            let _ = &columns[to].push(*x);
        }
    }

    let res = columns.iter()
        .map(|x1| {x1.last().unwrap().to_string()})
        .collect::<Vec<String>>()
        .join("");

    println!("{res:?}");
}

pub fn day_5_2() {
    let contents = fs::read_to_string("src/day_5/input.txt")
        .expect("Should have been able to read the file");
    let (crates, instructions) = contents.split_once("\n\n").unwrap();

    let mut columns = get_columns(crates);
    let instructions = parse_instructions(instructions);

    for [amount, from, to] in instructions {
        let from_length = columns[from].len();

        let x: &Vec<_> = &columns[from].drain( from_length - amount..)
            .collect();
        let _ = &columns[to].extend(x);
    }

    let res = columns.iter()
        .map(|x1| {x1.last().unwrap().to_string()})
        .collect::<Vec<String>>()
        .join("");

    println!("{res:?}");
}