use std::fs;

fn parse_limits(elf: &str) -> [i32; 2] {
    let (first, second) = elf.split_once("-").unwrap();
    let a = first.parse().unwrap();
    let b = second.parse().unwrap();

    return [a, b];
}

fn is_fully_contained(la: [i32; 2], lb: [i32; 2]) -> bool {
    if la[0] <= lb[0] && la[1] >= lb[1] {
        true
    } else if la[0] >= lb[0] && la[1] <= lb[1] {
        true
    } else {
        false
    }
}

fn partially_overlaps(la: [i32; 2], lb: [i32; 2]) -> bool {
    if is_fully_contained(la, lb) {
        true
    } else if la[0] >= lb[0] && la[0] <= lb[1] {
        true
    } else if la[1] >= lb[0] && la[1] <= lb[1] {
        true
    } else {
        false
    }
}

pub fn day_4() {
    let contents = fs::read_to_string("src/day_4/input.txt")
        .expect("Should have been able to read the file");

    let lines = contents.split("\n");

    let mut cases = 0;

    for line in lines {
        let (first, second) = line.split_once(",").unwrap();

        let limits_first = parse_limits(first);
        let limits_second = parse_limits(second);

        if is_fully_contained(limits_first, limits_second) {
            cases = cases + 1;
        }
    }

    println!("{cases}");
}

pub fn day_4_2() {
    let contents = fs::read_to_string("src/day_4/input.txt")
        .expect("Should have been able to read the file");

    let lines = contents.split("\n");

    let mut cases = 0;

    for line in lines {
        let (first, second) = line.split_once(",").unwrap();

        let limits_first = parse_limits(first);
        let limits_second = parse_limits(second);

        if partially_overlaps(limits_first, limits_second) {
            cases = cases + 1;
        }
    }

    println!("{cases}");
}