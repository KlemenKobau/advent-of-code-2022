use std::collections::HashMap;
use std::fs;

pub fn day_7() {
    let contents = fs::read_to_string("src/day_7/input.txt")
        .expect("Should have been able to read the file");
    let lines = contents.split("\n");

    let mut current_path = Vec::new();
    current_path.push("");

    let mut folder_map = HashMap::new();

    for line in lines {
        let split = line.split(" ").collect::<Vec<&str>>();

        if split[0] == "$" && split[1] == "cd" {
            let cd_command = split[2];
            match cd_command {
                "/" => {
                    current_path.clear();
                    current_path.push("");
                }
                ".." => {
                    current_path.pop();
                }
                l => {
                    current_path.push(l);
                }
            }
        } else if split[0] == "$" && split[1] == "ls" {
            // ignore
        } else if split[0] == "dir" {
            // ignore
        } else {
            let file_size: i32 = split[0].parse().unwrap();

            for i in 0..current_path.len() {
                let path = current_path[0..i + 1].join("/");

                let item = folder_map.get(&path);
                match item {
                    Some(t) => {
                        folder_map.insert(path, t + file_size);
                    },
                    None => {
                        folder_map.insert(path, file_size);
                    }
                }
            }
        }
    }

    // part 1
    let combined: Option<i32> = folder_map.iter()
        .map(|(_, size)| {*size})
        .filter(|&size| { size <= 100000 })
        .reduce(|x, x1| { x + x1 });

    // println!("{}", combined.unwrap());

    // part 2
    let disk_space = 70000000;
    let required_space = 30000000;
    let used_space = folder_map.get("").unwrap();

    let unused_space = disk_space - used_space;
    let to_free = required_space - unused_space;

    let mut folder_list = folder_map.into_iter().collect::<Vec<_>>();
    folder_list.sort_unstable_by(|(_, size1), (_, size2)| {size1.cmp(size2)});

    let res = folder_list.iter().map(|&(_, size)| {size}).find(|&size| { size > to_free }).unwrap();

    println!("{:?}", res);
}
